---
layout: docs
title: Download
---

## Public key
Packages and repositories are signed with the key [`FFAB E317 A779 4409 3EDE EFBF B14E 23A1 3282 A79F`](https://pgp.mit.edu/pks/lookup?op=get&search=0xB14E23A13282A79F).
You can [download the key]({% link assets/alt.key %}).

## Latest releases

|              | Version                                                                                                | Release date                                                   |
| ------------ | ------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------- |
| **stable**   | [{{page.release.stable.version}}  ]({{site.gitlab.core.link}}/tags/{{page.release.stable.commit}})     | {{page.release.stable.date   | date: "%Y-%m-%d %H:%M:%S %z" }} |
| **snapshot** | [{{page.release.snapshot.version}}]({{site.gitlab.core.link}}/commit/{{page.release.snapshot.commit}}) | {{page.release.snapshot.date | date: "%Y-%m-%d %H:%M:%S %z"}}  |

We only release snapshot packages at the moment. However, we plan to release full tagged releases [soon(TM)](https://wowwiki.fandom.com/wiki/Soon).

## Install using package manager
The following packages are available:
 - `algorithms-library-snapshot` (core, includes `aql2` executable)
 - `algorithms-library-dev-snapshot` (development headers for debian/ubuntu)

### ArchLinux

Append the following lines to your `/etc/pacman.conf`:
{% highlight shell %}
# Algorithms Library repository
[algorithms-library]
Server = {{site.urlrepo}}/archlinux/$arch/
{% endhighlight %}

And update the database.

{% highlight shell %}
pacman -Sy
{% endhighlight %}

Do not forget to verify the fingerprint when importing the key.
You will probably need to sign the key locally using `pacman-key --lsign-key`.

### Debian and Ubuntu
Append the following line in your `/etc/apt/sources.list` (or create a file inside your `sources.d` directory) and replace `<distribution>` with your Debian/Ubuntu distribution codename.

{% highlight shell %}
deb {{site.urlrepo}}/debian <distribution> main
{% endhighlight %}

We maintain repositories for the following versions of Debian/Ubuntu:

| `<distribution>` | Description                        |
| ---------------- | ---------------------------------- |
| `bullseye`       | Debian 11 (testing, _bullseye_)    |
| `sid`            | Debian sid (unstable, _sid_)       |
| `focal`          | Ubuntu 20.04 LTS (_focal fossa_)   |
| `groovy`         | Ubuntu 20.10 (_groovy gorilla_)    |

Download the repository key and update repositories.

{% highlight shell %}
curl {{ "assets/alt.key" | absolute_url }} | apt-key add -
apt-get update
{% endhighlight %}

### Gentoo
Gentoo ebuild can be found [here](https://github.com/trpasle/trpasle-gentoo-overlay/tree/master/sci-misc/alt).

## Docker images
We also maintain docker images and publish them to our [gitlab registry]({{site.gitlab.core.link}}/container_registry).

Replace `<tag>` with values from the table below.

{% highlight shell %}
docker pull gitlab.fit.cvut.cz:5000/algorithms-library-toolkit/automata-library:<tag>
docker run -it gitlab.fit.cvut.cz:5000/algorithms-library-toolkit/automata-library[:tag]
{% endhighlight %}

| tag          | Description                                     |
| ------------ | ----------------------------------------------- |
| `latest`     | latest stable release _(default tag)_           |
| `snapshot`   | latest snapshot release                         |
| `v[version]` | stable release, e.g. `v1.2.3` for 1.2.3 release | 

## Install from sources
You can grab sources from our
[git repository]({{site.gitlab.core.link}})
or from
[tarball repository]({{site.urlrepo}}/sources)

