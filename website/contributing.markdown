---
layout: docs
title: Contributing
---

## Fork
ALT is licensed with GPL3. Feel free to fork it. However, we will be happy if you share your fixes or features with us.

## Issues
The issue tracker is available at [our gitlab page]({{site.gitlab.core.link}}/issues).

## Theses for FIT students
We have few ideas that might result in bachelor/master theses for [FIT students](https://fit.cvut.cz). The list below is not complete. We are open to your ideas. Feel free to contact us or visit us at room A1246 and we will discuss details.

* Study and implement a nontrivial algorithm from the automata theory (pattern matching, ...)
* Extension of `aql` language (procedures, loops, ...)
* Extension (or a rework) of our GUI interface `agui`
* Web interface for ALT
* CI/CD: Implementation of a good CI/CD pipeline using Gitlab CI. Automated packaging for predefined Linux distributions and Docker images
