require 'open-uri'
require 'date'

class AltVersion < Jekyll::Generator
  TARBALL_REGEX = /<a href="(.*.tar.gz)">.*<\/a>\s+(\d+)-(\w+)-(\d+)\s+(\d+):(\d+)\s+\w+/
  VERSION_REGEX = /algorithms-library_(.*).tar.gz/

  class Entry
    def initialize(version, commit, day, month, year, hour, min)
      @version = version
      @commit = commit
      @date = DateTime.strptime("%s-%s-%s %s:%s:00+0000" % [year, month, day, hour, min], "%Y-%b-%d %H:%M:%S%z")
    end

    def version
      @version
    end

    def date
      @date
    end

    def commit
      @commit
    end
  end

  def generate(site)
    tmp = self.fetch('https://alt.fit.cvut.cz/repo/sources/')
    r = tmp["stable"]
    s = tmp["snapshot"]

    site.pages.each do |page|
      page.data['release'] = {}

      if s.nil?
        page.data['release']['snapshot'] = { 'version' => 'N/A', 'date' => 'N/A', 'commit' => nil }
      else
        page.data['release']['snapshot'] = { 'version' => s.version, 'date' => s.date, 'commit' => s.commit }
      end

      if r.nil?
        page.data['release']['stable'] = { 'version' => 'N/A', 'date' => 'N/A', 'commit' => nil }
      else
        page.data['release']['stable'] = { 'version' => r.version, 'date' => r.date, 'commit' => r.commit }
      end
    end
  end

  def fetch(url)
    html = URI.open(url).read
    snapshots = []
    releases = []

    html.scan(TARBALL_REGEX) do |file, day, month, year, hour, min|
      version = file.match(VERSION_REGEX).captures[0]
      if version.scan(/r\d+/) # snapshot, has rXXX
        commit = version.match(/r\d+\.g(\w+)/).captures
        snapshots << Entry.new(version, commit, day, month, year, hour, min)
      else
        commit = version
        releases << Entry.new(version, commit, day, month, year, hour, min)
      end
    end

    snapshots = snapshots.sort_by(&:version).reverse
    releases = releases.sort_by(&:version).reverse

    {
      "snapshot" => snapshots.empty? ? nil : snapshots[0],
      "stable"   => releases.empty?  ? nil : releases[0]
    }
  end
end
