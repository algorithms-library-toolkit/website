class BreadcrumbsGenerator < Jekyll::Generator
  def generate(site)
    site.pages.each do |page|
      url_parts = page.url.split("/")

      hierarchy = []
      hierarchy << "/"
      for i in 1..url_parts.size - 1
        hierarchy << url_parts[0..i].join("/")
      end
      page.data["breadcrumbs"] = hierarchy.map { |url| find_by_url(site, url) }
    end
  end

  def find_by_url(site, url)
    site.pages.each do |page|
      if page.url.delete_suffix("/") == url.delete_suffix("/")
        return page
      end
    end

    puts "Breadcrumbs: Url >%s< not found" % url
    return nil
  end
end
