---
layout: docs
title: Components of ALT
---
## Overview

Each of the following components represents one directory in the source code structure. Most of them result in dynamically linked libraries.

| Compoment                  | Type       | Description |
| -------------------------- | ---------- | ----------- |
| `agui2`                    | Executable | GUI entrypoint |
| `alib2abstraction`         | Library    |             |
| `alib2algo`                | Library    |             |
| `alib2algo_experimental`   | Library    |             |
| `alib2aux`                 | Library    |             |
| `alib2cli`                 | Library    |             |
| `alib2common`              | Library    |             |
| `alib2data`                | Library    |             |
| `alib2data_experimental`   | Library    |             |
| [`alib2dummy`]({% link docs/alib2dummy.markdown %}) | Library    | Sample datatype and algorithm |
| `alib2elgo`                | Library    |             |
| `alib2graph_algo`          | Library    |             |
| `alib2graph_data`          | Library    |             |
| `alib2gui`                 | Library    |             |
| `alib2integrationtest`     | Library    |             |
| `alib2measure`             | Library    |             |
| `alib2raw`                 | Library    |             |
| `alib2raw_cli_integration` | Library    |             |
| `alib2std`                 | Library    |             |
| `alib2str`                 | Library    |             |
| `alib2str_cli_integration` | Library    |             |
| `alib2xml`                 | Library    |             |
| `aql2`                     | Executable | Command-line entrypoint |
