---
layout: docs
title: Documentation
---

## Basic
* [quick start with examples]({% link docs/quickstart.markdown %})
* [user guide]({% link docs/userguide.markdown %})
* [the language of string::Parse algorithm]({% link docs/parse.markdown %})

## Installation
* [downloading]({% link download.markdown %})
* [installing from sources]({% link docs/install.markdown %})
* [versioning]({% link docs/versioning.markdown %})

## Advanced
* [aql language in detail]({% link docs/querylanguage.markdown %})
* [components of ALT]({% link docs/components.markdown %})
* [adding a datatype or an algorithm]({% link docs/alib2dummy.markdown %})

## API documentation
* [api](/apidoc/snapshot/)
