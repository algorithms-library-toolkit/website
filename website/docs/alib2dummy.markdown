---
layout: docs
title: alib2dummy 
---

We have prepared the `alib2dummy` library to be a starting point for your first addition to the project. It contains an example datatype and algorithm for you to play with. 

You can (re)use `alib2dummy` for your experiments with ALT straight away.

## Example of adding your datatype and algorithm
The `alib2dummy` library features a playground so you can easily develop your new datatypes and/or algorithms.
In the `alib2dummy/src/example` folder you shall find a simple datatype called `DataType` and `CreateDataType` algorithm.
The unit tests for this datatype and algorithm is located in the `alib2dummy/test-src/example` folder.

### Example datatype
The `DataType` class is really simple and not worth of any explaining. The important part is located in `DataType.cpp` file.
The following excerpt of code registers the `std::ostream& operator<< ( std::ostream&, const example::DataType& )` function as a ValuePrinter for `DataType` class.
This is used for printing in the CLI. You can see more examples of registering ValuePrinters in `PrimitiveRegistrator.cpp` file.

```cpp
auto valuePrinter = registration::ValuePrinterRegister < example::DataType > ( );
```

In the `xml` subfolder you can also see how to implement and register simple `XML` serializator and deserializator for your datatype.

### Example algorithm
The algorithms we present in `CreateDataType` module are (again) very simple. They just create the `DataType` object.
The important lesson is that all the algorithms are implemented as `public static` functions in the class.

In order to register the algorithms (for use in CLI, webUI, ...) see the following excerpt from `CreateDataType.cpp`:

```cpp
auto create1 = registration::AbstractRegister < example::CreateDataType, example::DataType > ( example::CreateDataType::create );
auto create2 = registration::AbstractRegister < example::CreateDataType2, example::DataType, int > ( example::CreateDataType2::create );
auto create3 = registration::AbstractRegister < example::CreateDataType2, example::DataType, double > ( example::CreateDataType2::create );
```

The lines here represent registrations of three algorithms (it can also be 3 different registrations or the same algorithm) specified in the parentheses. The template parameters to `AbstractRegister` are as follows:

 - Name of the algorithm class. This will be the name of the algorithm in the CLI.
 - Return type. All our algorithms return an object of `example::DataType` class
 - Types of parameters. The first algorithm has no parameters, the following algorithms accept one int and one double, respectively. Note that the registration with `double` parameter actually calls the templated `create` function.

You can now verify the registration in the CLI using `introspect algorithms` or `introspect overloads` commands. If you do not see the algorithms there, check that you have linked `aql` against `alib2dummy` library or preload the shared library using something like `LD_PRELOAD=libalib2dummy.so aql2`.

There are also some overloads for the registration. You can specify the documentation and names of the parameters so the CLI's `introspect` commands can help tell you more. You can set the docs like this:
```cpp
auto create3 = registration::AbstractRegister < example::CreateDataType2, example::DataType, double > ( example::CreateDataType2::create, "val" ).setDocumentation("Creates an object of DataType class with specified value\n\n@param val value passed to DataType\n@return DataType object with its value equal to val";
```

