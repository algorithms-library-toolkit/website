---
layout: docs
title: Installing from sources
---

## Prerequisites

### Build dependencies

* `cmake >= 3.7` (you can use both `ninja` and `make`)
* `python >= 3.4`
* `g++ >= 10.0.0` or `clang++ >= 9.0` or another `c++20` compatible compiler
* `tclap` (argument parsing)
* `libxml2`
* `readline`

Additionally, for building also the GUI part (deprecated in favour of WebUI, standard build does not include this as of Sep 2020), you will need
* `qt5-qtbase >= 5.7`
* `jsoncpp`
* `graphviz`

### Runtime dependencies
* `libxml2`
* `readline`
* `qt5 >= 5.7` (for GUI only)
* `jsoncpp` (for GUI only)
* `graphviz` (for GUI only, optional for CLI)

## Prepare

### Bash helper script
U can take a look at `./build.sh` bash script does all the work for you.
Run with `./build.sh release` will prepare and compile ALT into `release` directory.
`aql2` is then accessible in the `release/aql2` directory.

### CMake
ALT is built by [CMake](https://cmake.org).

Simply create a `build` directory, navigate into it and run `cmake /path/to/alt/sources`.
There are some ALT-specific options that you can pass to `cmake`:

| Option                     | Description                                    |
| -------------------------- | ---------------------------------------------- |
| `-DWITH_DOCS=<ON|OFF>`     | Create `doxygen` target for creating api docs. |
| `-DWITH_AGUI=<ON|OFF>`     | Compile GUI (deprecated).                      |

Of course you can use other CMake generic arguments, like `-GNinja`, `-DBUILD_TESTING`, `-DCMAKE_BUILD_TYPE`, `-DCMAKE_INSTALL_PREFIX`, etc.
Refer to CMake documentation for more information.

After you did that, you can now run `make` and possibly `make tests` and `make install DESTDIR=/usr/local`.
Doxygen job can be run with `make doxygen`. The HTML documentation output directory is `/apidoc`
In case you selected `ninja`, you have to call `ctest .` instead of `make tests`.
