---
layout: docs
title: Versioning
---

## Glossary

|               | Description     |
| ------------- | --------------- |
| `major`       | A major version |
| `minor`       | A minor version | 
| `patch`       | A patch version |
| `commits`     | Number of commits since the stable version tag |
| `commit_hash` | Hash of the commit which was compiled |

## Stable versions
Stable versions are in the format `<major>.<minor>.<patch>`. Stable versions are [tagged in the repository]({{site.gitlab.core.link}}/tags?search=v&sort=updated_desc) with `v<major>.<minor>.<patch>` tag.

The version is configured in `CMake/generate.conf` and inserted into the root `CMakeLists.txt` file when the file is generated.

## Snapshot versions
Snapshot versions corresponds to the following format: `<major>.<minor>.<patch>.r<commits>.g<commit_hash>`

The current snapshot version string can be obtained from git.
```bash
git describe --long --tags --match="v*" |  sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
```

