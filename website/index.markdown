---
layout: default
title: Algorithms Library Toolkit
navigation: false
---

<div class="container">

<div class="jumbotron">
  <h1>Algorithms Library Toolkit</h1>

  <p class="lead" markdown=1>
  Algorithms Library Toolkit (ALT) is a tool for manipulating with data structures from the area of stringology.
  It supports data structures like various kinds of automata, grammars, trees, etc. Also, many algorithms on these data structures are available.
  </p>

  <p markdown=1>
  ALT can also serve as a teaching tool in formal language and string processing courses.
  It comes with a command line interface (`aql2`), a prototype of a GUI version (`agui2`), and a client that runs in your browser.
  </p>

  <p markdown=1>
  ALT is under [active development]({{site.gitlab.core.link}}/commits/master) at [Faculty of Information Technology](https://fit.cvut.cz), Czech Technical University in Prague.
  </p>

  <p>
  <a class="btn btn-primary btn-md" href="{% link download.markdown %}"   role="button">Get ALT</a>
  <a class="btn btn-primary btn-md" href="{% link docs/quickstart.markdown %}" role="button">Quickstart</a>
  <a class="btn btn-primary btn-md" href="{{site.webui.link}}" role="button">Try webui</a>
  </p>

</div>
</div>
