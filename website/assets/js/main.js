$(function () {
  function initSearchBox() {
    var pages = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("title"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,

	  prefetch: { url: '/search.json', cache: false },
    });

    $('#search-box').typeahead({
      minLength: 1,
      highlight: true
    }, {
        name: 'pages',
        display: 'title',
        source: pages
      });

    $('#search-box').bind('typeahead:select', function (ev, suggestion) {
      window.location.href = suggestion.url;
    });
  }

  initSearchBox();
});
