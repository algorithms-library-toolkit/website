#!/usr/bin/env bash

REPOSITORY="https://alt.fit.cvut.cz/repo/apidoc/"
SNAPSHOT_REGEX="r[0-9][0-9]*.g"

VERSIONS=$(curl $REPOSITORY 2>/dev/null | grep apidoc.tar.gz | sed 's|.*algorithms-library_\(.*\)-apidoc.tar.gz.*|\1|')

# there should always be only one, just be safe
REL_SNAP=$(echo "$VERSIONS" | grep    "$SNAPSHOT_REGEX" | sort | tail -n 1 | tr "\n" " ")
REL_STAB=$(echo "$VERSIONS" | grep -v "$SNAPSHOT_REGEX" | sort | tail -n 2 | tr "\n" " ")

# $1 ... version
# $2 ... rename to...
fetch () {
	set -e
	dir="algorithms-library_$1-apidoc"

	echo " - wget $REPOSITORY/$dir.tar.gz"
	wget -q $REPOSITORY/$dir.tar.gz

	echo " - tar xzf $dir.tar.gz"
	tar xzf $dir.tar.gz

	echo " - mv $dir $2"
	mv $dir $2

	echo " - rm $dir.tar.gz"
	rm "$dir.tar.gz"
	set +e
}

if [[ ! -z "$REL_SNAP" ]]; then
	for version in "$REL_SNAP"; do
		echo "Fetch snapshot $version"
		fetch $version snapshot
	done
fi

if [[ ! -z "$REL_STAB" ]]; then
	for version in "$REL_STAB"; do
		echo "Fetch stable $version"
		fetch $version $version
	done
fi
