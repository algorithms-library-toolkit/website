FROM gitlab.fit.cvut.cz:5000/algorithms-library-toolkit/website/jekyll-builder AS builder-jekyll

ARG environment="production"
ENV JEKYLL_ENV=${environment}

ADD . /app
WORKDIR /app
RUN bundler exec jekyll build --trace --config _config.yml


# -----------------------------------------------------------------------------

FROM alpine:latest AS builder-apidoc

RUN apk add --no-cache tar wget curl bash
ADD apidoc.sh /
WORKDIR /apidoc
RUN ../apidoc.sh /apidoc

# -----------------------------------------------------------------------------

FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder-jekyll /app/_site /usr/share/nginx/html
COPY --from=builder-apidoc /apidoc    /usr/share/nginx/html/apidoc
